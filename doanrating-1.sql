-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2021 at 11:03 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doanrating`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user`, `password`) VALUES
('admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `binhluan`
--

CREATE TABLE `binhluan` (
  `MABL` int(11) NOT NULL,
  `MAMH` int(11) NOT NULL,
  `MAKH` int(11) NOT NULL,
  `NGAYBL` date NOT NULL,
  `NOIDUNG` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `binhluan`
--

INSERT INTO `binhluan` (`MABL`, `MAMH`, `MAKH`, `NGAYBL`, `NOIDUNG`) VALUES
(1, 26, 1, '2021-11-17', '  \r\nhello\r\n                    '),
(8, 26, 1, '2021-11-17', '  \r\nhello\r\n                    ');

-- --------------------------------------------------------

--
-- Table structure for table `chitiethd`
--

CREATE TABLE `chitiethd` (
  `MASOHD` int(11) NOT NULL,
  `MAMH` int(11) NOT NULL,
  `SOLUONGMUA` int(11) NOT NULL,
  `SIZE` varchar(10) NOT NULL,
  `MAUSAC` varchar(10) NOT NULL,
  `THANHTIEN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chitiethd`
--

INSERT INTO `chitiethd` (`MASOHD`, `MAMH`, `SOLUONGMUA`, `SIZE`, `MAUSAC`, `THANHTIEN`) VALUES
(8, 12, 1, '39', 'Màu Đen', 300000),
(9, 7, 1, '38', 'Màu Đen', 280000),
(9, 12, 1, '39', 'Màu Đen', 300000),
(10, 7, 1, '38', 'Màu Đen', 280000),
(10, 12, 1, '39', 'Màu Đen', 300000),
(11, 21, 1, '39', 'Màu Xanh', 270000),
(11, 24, 1, '37', 'Màu Vàng', 400000),
(12, 14, 1, '39', 'Màu Đen', 180000),
(13, 18, 1, '37', 'Màu Xanh', 275000),
(14, 18, 1, '37', 'Màu Đen', 275000),
(15, 4, 1, '40', 'Màu Đỏ', 279000),
(15, 10, 1, '40', 'Màu Hồng', 129000),
(15, 18, 1, '37', 'Màu Đen', 275000),
(16, 10, 1, '40', 'Màu Hồng', 129000),
(16, 18, 1, '37', 'Màu Đen', 275000),
(17, 10, 1, '40', 'Màu Hồng', 129000),
(17, 18, 1, '37', 'Màu Đen', 275000),
(18, 10, 1, '40', 'Màu Hồng', 129000),
(18, 18, 1, '37', 'Màu Đen', 275000),
(19, 8, 1, '38', 'Màu Vàng', 273000),
(19, 10, 1, '40', 'Màu Hồng', 129000),
(19, 18, 999, '37', 'Màu Đen', 275000),
(20, 25, 1, '37', 'Màu Vàng', 280000),
(21, 14, 1, '39', 'Màu Đen', 180000),
(22, 20, 1, '39', 'Màu Xanh', 500000),
(23, 19, 1, '37', 'Màu Xanh', 300000),
(25, 24, 1, '37', 'Màu Vàng', 400000),
(26, 11, 1, '39', 'Màu Xanh', 373000),
(27, 11, 1, '39', 'Màu Xanh', 373000),
(27, 19, 1, '37', 'Màu Xanh', 300000),
(28, 3, 1, '40', 'Màu Đỏ', 179000),
(28, 19, 1, '37', 'Màu Xanh', 300000);

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `MASOHD` int(11) NOT NULL,
  `MAKH` int(11) NOT NULL,
  `NGAYDAT` date NOT NULL,
  `TONGTIEN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hoadon`
--

INSERT INTO `hoadon` (`MASOHD`, `MAKH`, `NGAYDAT`, `TONGTIEN`) VALUES
(1, 2, '2021-10-20', 239000),
(2, 2, '2021-10-21', 160000),
(3, 2, '2021-10-21', 160000),
(4, 2, '2021-10-21', 160000),
(5, 2, '2021-10-21', 160000),
(6, 2, '2021-10-21', 160000),
(7, 2, '2021-10-21', 160000),
(8, 2, '2021-10-21', 300000),
(9, 2, '2021-10-21', 580000),
(10, 2, '2021-10-21', 580000),
(11, 2, '2021-10-21', 670000),
(12, 2, '2021-10-21', 180000),
(13, 2, '2021-10-21', 275000),
(14, 2, '2021-10-21', 275000),
(15, 2, '2021-10-21', 683000),
(16, 2, '2021-10-21', 404000),
(17, 2, '2021-10-21', 404000),
(18, 2, '2021-10-21', 404000),
(19, 2, '2021-10-21', 677000),
(20, 2, '2021-10-21', 280000),
(21, 2, '2021-10-21', 180000),
(22, 2, '2021-10-22', 500000),
(23, 2, '2021-10-30', 300000),
(24, 2, '2021-11-11', 500000),
(25, 2, '2021-11-15', 400000),
(26, 2, '2021-11-17', 373000),
(27, 2, '2021-11-17', 673000),
(28, 2, '2021-11-17', 479000);

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `MAKH` int(11) NOT NULL COMMENT '	',
  `TENKH` varchar(100) NOT NULL,
  `DIACHI` varchar(100) NOT NULL,
  `DIENTHOAI` varchar(15) NOT NULL,
  `TENTK` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `EMAIL` varchar(250) NOT NULL,
  `PASS` varchar(250) NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MAKH`, `TENKH`, `DIACHI`, `DIENTHOAI`, `TENTK`, `EMAIL`, `PASS`, `avatar`) VALUES
(1, 'truong', 'gia lai', '2321412', 'habazaki', 'habazaki@gmail.com', '202cb962ac59075b964b07152d234b70', ''),
(2, 'truong1', 'gia lai', '2321412', 'truong', 'quoctruongvo2001@gmail.com', '202cb962ac59075b964b07152d234b70', 'CORR0758.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `loai`
--

CREATE TABLE `loai` (
  `MALOAI` int(11) NOT NULL,
  `TENLOAI` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `IDMENU` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loai`
--

INSERT INTO `loai` (`MALOAI`, `TENLOAI`, `IDMENU`) VALUES
(1, 'Áo Hoddie(Nam)', 1),
(2, 'Áo Hoddie(Nữ)', 1),
(3, 'Quần kaki(Nam)', 2),
(4, 'Quần joker(Nữ)', 2),
(5, 'Quần thể thao(Nam)', 2),
(6, 'Quần thể thao nữ caro(Nữ)', 2),
(7, 'Áo tay lỡ(Nam)', 3),
(8, 'Quần thể thao nữ có kẻ sọc(Nữ)', 2),
(10, 'Áo tay lỡ(Nữ)', 3);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `ID` int(11) NOT NULL,
  `TEN` varchar(255) NOT NULL,
  `LINK` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE `sanpham` (
  `MAMH` int(50) NOT NULL,
  `TENMH` varchar(255) NOT NULL,
  `SOLUONG` int(11) NOT NULL,
  `SLTON` int(11) NOT NULL,
  `DONGIA` int(11) NOT NULL,
  `IMAGES` varchar(50) NOT NULL,
  `MOTA` text NOT NULL,
  `LOAI` varchar(255) NOT NULL,
  `MALOAI` int(11) NOT NULL,
  `SIZE` varchar(10) NOT NULL,
  `MAUSAC` varchar(10) NOT NULL,
  `NGAYDANG` date NOT NULL,
  `LOAISP` varchar(200) NOT NULL,
  `GIAMGIA` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`MAMH`, `TENMH`, `SOLUONG`, `SLTON`, `DONGIA`, `IMAGES`, `MOTA`, `LOAI`, `MALOAI`, `SIZE`, `MAUSAC`, `NGAYDANG`, `LOAISP`, `GIAMGIA`) VALUES
(1, 'Áo Khoác Hoodie SENTI Ulzzang Unisex 1hitshop', 999, 10, 300000, 'aokhoacnam5.jpg', 'DÂY KÉO: CÙNG MÀU VẢI \r\nChất vải:  Nỉ bông (nên có dính lông chút bên trong )\r\nMàu TRẮNG: Chất vải DÀY hơn ĐEN và XÁM \r\nMàu ĐEN - XÁM: Vải MỀM MỊN hơn ', '1', 1, '40', 'Màu Xanh', '2021-03-10', 'Áo Khoác ', 249000),
(2, 'Áo Khoác Hoodie SENTI Ulzzang Unisex 1hitshop', 10, 10, 310000, 'aokhoacnam6.jpg', 'DÂY KÉO: CÙNG MÀU VẢI \r\nChất vải:  Nỉ bông (nên có dính lông chút bên trong )\r\nMàu TRẮNG: Chất vải DÀY hơn ĐEN và XÁM \r\nMàu ĐEN - XÁM: Vải MỀM MỊN hơn', '1', 1, '39', 'Màu Xanh', '2021-03-10', 'Áo Khoác', 0),
(3, 'Áo Khoác Hoodie SENTI Ulzzang Unisex 2hitshop', 10, 10, 179000, 'aokhoacnam3.jpg', 'DÂY KÉO: CÙNG MÀU VẢI \r\nChất vải:  Nỉ bông (nên có dính lông chút bên trong )\r\nMàu TRẮNG: Chất vải DÀY hơn ĐEN và XÁM \r\nMàu ĐEN - XÁM: Vải MỀM MỊN hơn', '2', 1, '40', 'Màu Đỏ', '2021-03-10', 'Áo khoác', 0),
(4, 'Áo khoas Hoodie Thêu MOOS  Xẻ Tà Ulzzang Unisex ', 10, 10, 279000, 'aokhoacnam4.jpg', 'Áo Khoác Nỉ Bông Thêu MOOS Form Rộng Xẻ Tà Ulzzang Unisex (Ánh Thật), Áo khoác nam nữ thể thao K36, Sweater hoodie hot\r\nNghe nói hôm nay trời lạnh hay sao vậy các bác ơiii??? Bên shop đã kĩ càng về thêm đủ màu các mẫu áo khoác nỉ từ mỏng tới dày giữ ấm ', '2', 1, '40', 'Màu Xanh', '2021-03-10', 'Áo Khoác', 179000),
(5, 'Áo Khoác Nỉ Bông Thêu MOOS Xẻ Tà Ulzzang Unisex ', 10, 10, 270000, 'aokhoacnam1.jpg', 'Áo Khoác Nỉ Bông Thêu MOOS Form Rộng Xẻ Tà Ulzzang Unisex (Ánh Thật), Áo khoác nam nữ thể thao K36, Sweater hoodie hot\r\nNghe nói hôm nay trời lạnh hay sao vậy các bác ơiii??? Bên shop đã kĩ càng về thêm đủ màu các mẫu áo khoác nỉ từ mỏng tới dày giữ ấm ', '3', 1, '39', 'Màu Cam', '2021-10-04', 'Áo Khoác', 0),
(6, 'Áo Khoác Nỉ Bông Thêu MOOS Form Rộng Xẻ Tà Ulzzang Unisex ', 10, 10, 239000, 'aokhoacnam2.jpg', 'Áo Khoác Nỉ Bông Thêu MOOS Form Rộng Xẻ Tà Ulzzang Unisex (Ánh Thật), Áo khoác nam nữ thể thao K36, Sweater hoodie hot\r\nNghe nói hôm nay trời lạnh hay sao vậy các bác ơiii??? Bên shop đã kĩ càng về thêm đủ màu các mẫu áo khoác nỉ từ mỏng tới dày giữ ấm ', '4', 1, '39', 'Màu Xám', '2021-10-04', 'Áo Khoác', 0),
(7, 'Áo Khoác Hoddie nữ Unisex Tom & Jerry', 10, 10, 280000, 'aokhoacnu2.jpg', 'Áo khoác mũ lông logo thêu Tom & jerry from rộng kute siêu hot. Không những đẹp mà còn ấm áp trong mùa đông. Diện Em nó siêu dễ thương như những chú Gấu thực thụ.', '5', 2, '38', 'Màu Đen', '2021-03-10', 'Áo Khoác', 173000),
(8, 'Áo Hodde Nữ Thêu hình Gấu Teddy xinh xắn', 10, 10, 273000, 'aokhoacnu1.jpg', 'Áo khoác hoddie thêu hình gấu  Áo khoác ulzzang Hoddie  nữ form rộng thể thao sale  freeship', '6', 2, '38', 'Màu Vàng', '2021-10-04', 'Áo Khoác', 179000),
(9, 'Áo khoác nam bomber varsity jacket ', 10, 10, 530000, 'aokhoacnam7.jpg', ' Form dáng chuẩn mẫu, thiết kế trẻ trung cá tính, năng động, chuẩn style hàn quốc, áo cả nam nữ đều mặc được', '7', 1, '39', 'Màu Xanh', '2021-10-04', 'Áo Khoác', 0),
(10, 'ÁO KHOÁC HOODIE Nam NÚT GÀI THÊU HÌNH PHONG CÁCH', 10, 10, 129000, 'aokhoacnam8.jpg', 'Kiểu dáng: Hàn Quốc do những nhà thiết kế chuyên nghiệp được đào tạo trực tiếp từ xứ sở thời trang', '8', 1, '40', 'Màu Hồng', '2021-10-04', 'Áo Khoác', 0),
(11, 'Quần Kaki Joker Túi Hộp Cao Cấp Cực Đẹp', 10, 10, 373000, 'quannam5.jpg', 'Quần Kaki Joker Túi Hộp Cao Cấp Cực Đẹp Có sẵn trên TNG-Shop', '9', 3, '39', 'Màu Xanh', '2021-03-10', 'Quần', 225000),
(12, 'Quần thể thao ống rộng thời trang mùa thu mới 2021', 10, 10, 300000, 'quannam1.jpg', 'Quần thể thao ống rộng thời trang có các đường nét caro sọc ngang xen lẫn màu sắc hòa quyện tinh tế\r\n', '10', 5, '39', 'Màu Xám', '2021-10-04', 'Quần', 0),
(13, 'Quần thể thao ống rộng thời trang mùa thu mới 2021', 10, 10, 280000, 'quannam3.jpg', 'Quần thể thao ống rộng thời trang có các đường nét caro sọc ngang xen lẫn màu sắc hòa quyện tinh tế', '10', 5, '40', 'Màu Trắng', '2021-10-04', 'Quần', 173000),
(14, 'Quần thể thao ống rộng thời trang mùa thu mới 2021', 10, 10, 180000, 'quannam4.jpg', 'Quần thể thao ống rộng thời trang có các đường nét caro sọc ngang xen lẫn màu sắc hòa quyện tinh tế', '10', 5, '39', 'Màu Đen', '2021-10-04', 'Quần', 0),
(15, 'Quần thể thao nam đơn giản', 10, 10, 270000, 'quannam2.jpg', 'Quần nam thể thao mang chủ đề đơn giản với chất vài thun săn chắc thuận tiện trong việc vận động mạng bạo lực', '11', 5, '38', 'Màu Đen', '2021-10-04', 'Quần', 173000),
(16, 'Quần joker nữ 2 túi  bên hông', 10, 10, 300000, 'quannu1.jpg', 'Quần ống rộng lưng cao vải mềm', '12', 4, '37', 'Màu Trắng', '2021-10-04', 'Quần', 179000),
(17, 'Quần joker nữ 2 túi  bên hông', 10, 10, 160000, 'quannu4.jpg', 'Bạn muốn chọn 1 chiếc quần vải 70-80k kém chất lượng về không mặc nổi hay chọn 1 chiếc quần cao cấp với chất lượng hoàn toàn xứng đáng giúp bạn thoải mái và tự tin?', '12', 4, '38', 'Màu Nâu', '2021-10-04', 'Quần', 173000),
(18, 'Quần thể thao nữ caro xen kẽ', 10, 10, 275000, 'quannu3.jpg', 'FREESHIP cho tất cả các đơn hàng từ 150K\r\nCam kết chất lượng và mẫu mã sản phẩm giống với hình ảnh.\r\nHoàn tiền nếu sản phẩm không giống với mô tả.\r\nCam kết được đổi trả hàng trong vòng 2 ngày. \r\nShop hỗ trợ khách hàng đổi size sản phẩm.\r\n', '13', 6, '37', 'Màu Xanh', '2021-10-04', 'Quần', 0),
(19, 'Quần thể thao nữ có kẻ sọc trắng', 10, 10, 300000, 'quannu5.jpg', 'Thời gian giao hàng dự kiến cho sản phẩm này là từ 7-9 ngày\r\nMặc dù giá sản phẩm của chúng tôi không phải là thấp nhất nhưng chúng tôi đảm bảo đây là hàng chất lượng cao và chúng tôi sẽ cố gắng hết sức để đem đến cho bạn những sản phẩm tốt nhất', '13', 8, '37', 'Màu Tím', '2021-10-04', 'Quần', 129000),
(20, 'Áo tat lỡ Dệt Kim Tay Dài Cổ Chữ V Họa Tiết Hình Thoi Phong Cách Hàn Quốc', 10, 10, 500000, 'aonam2.jpg', ' Do hiệu ứng hiển thị và ánh sáng khác nhau nên màu sắc thực tế của sản phẩm có thể hơi khác so với màu trong hình. Cảm ơn bạn.\r\n  Nếu sản phẩm của chúng tôi không có kích thước, màu sắc yêu thích của bạn hoặc bạn muốn tìm hiểu thêm thông tin, vui lòng liên hệ với chúng tôi. Cảm ơn bạn.', '14', 7, '39', 'Màu Xanh', '2021-10-04', 'Áo', 0),
(21, 'Áo khoác tay lỡ thêu họa tiết thời trang Hàn Quốc', 10, 10, 270000, 'aonam3.jpg', 'Áo khoác bóng chày thêu họa tiết thời trang Hàn Quốc Dành Cho Nữ', '14', 7, '40', 'Màu Đen', '2021-10-04', 'Áo', 0),
(22, 'Áo tay lỡ nam họa tiết tinh tế', 10, 10, 300000, 'aonam1.jpg', 'ÁO THUN NAM CỘC TAY CÓ CỔ\r\nCam kết chất lượng và mẫu mã sản phẩm giống với hình ảnh. \r\nHoàn tiền nếu sản phẩm không giống với mô tả. \r\nCam kết được đổi trả hàng trong vòng 2 ngày. \r\n', '15', 7, '40', 'Màu Trắng', '2021-10-04', 'Áo', 150000),
(23, 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn', 10, 10, 350000, 'aonu1.jpg', 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn có đủ 4 màu đa dạng cho các chị em tín đồ của cái đẹp thỏa sức lựa chọn nha', '16', 10, '37', 'Màu Xanh', '2021-10-04', 'Áo', 250000),
(24, 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn', 10, 10, 400000, 'aonu2.jpg', 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn có đủ 4 màu đa dạng cho các chị em tín đồ của cái đẹp thỏa sức lựa chọn nha', '16', 10, '38', 'Màu Tím', '2021-10-04', 'Áo', 0),
(25, 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn', 10, 10, 280000, 'aonu3.jpg', 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn có đủ 4 màu đa dạng cho các chị em tín đồ của cái đẹp thỏa sức lựa chọn nha', '16', 10, '38', 'Màu Vàng', '2021-10-04', 'Áo', 0),
(26, 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn', 10, 10, 275000, 'aonu4.jpg', 'Áo thun tay lỡ nữ họa tiết bọt biển xinh xắn có đủ 4 màu đa dạng cho các chị em tín đồ của cái đẹp thỏa sức lựa chọn nha', '16', 10, '38', 'Màu Trắng', '2021-10-04', 'Áo', 0),
(27, 'Áo Tay lỡ nữ thời thượng 2022', 300, 250, 150000, 'aonu6.jpg', 'Áo thun tay lỡ nữ đằng cấp những chiết đường chỉ ngay ngắn tinh tế lỗi lạc từ nhà thiết kế TNG', '17', 10, '39', 'Màu Đỏ', '2021-10-20', 'Áo', 0),
(28, 'Áo thun Nữ Japan xinh xắn', 20, 20, 1000000, 'aonu7.jpg', 'Áo thun nữ xuất xứ từ nhật bản một kinh đồ thời trang tiềm năng trong tương lai với các họa tiết tưởng chừng đơn giản nhưng hóa ra đơn giản thiệt', '18', 10, '38', 'Màu Xanh', '2021-10-07', 'Áo', 999999),
(29, 'Áo thun Nữ họa tiết Disney', 20, 20, 650000, 'aonu5.jpg', 'Áo thun nữ xuất xứ từ nhật bản một kinh đồ thời trang tiềm năng trong tương lai với các họa tiết tưởng chừng đơn giản nhưng hóa ra đơn giản thiệt', '19', 10, '39', 'Màu Xanh', '2021-10-07', 'Áo', 0),
(30, 'Giảm 43 %】 Quần Jogger Thể Thao Nữ Phối Viền Beast - Krixi Fashion', 10, 10, 275000, 'quannu6.jpg', 'FREESHIP cho tất cả các đơn hàng từ 150K\r\nCam kết chất lượng và mẫu mã sản phẩm giống với hình ảnh.\r\nHoàn tiền nếu sản phẩm không giống với mô tả.\r\nCam kết được đổi trả hàng trong vòng 2 ngày. \r\nShop hỗ trợ khách hàng đổi size sản phẩm.\r\n', '13', 4, '38', 'Màu Đen', '2021-10-04', 'Quần', 200000);

-- --------------------------------------------------------

--
-- Table structure for table `stars`
--

CREATE TABLE `stars` (
  `productid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stars`
--

INSERT INTO `stars` (`productid`, `username`, `rating`) VALUES
(2, 'truong1', 4),
(3, 'truong1', 2),
(6, 'truong1', 3),
(14, 'truong1', 3),
(19, 'truong', 1),
(19, 'truong1', 3),
(20, 'truong1', 3),
(21, 'truong1', 2),
(22, 'truong1', 1),
(24, 'truong1', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `binhluan`
--
ALTER TABLE `binhluan`
  ADD PRIMARY KEY (`MABL`),
  ADD KEY `fk_bl_MAMH` (`MAMH`) USING BTREE,
  ADD KEY `fk_bl_MAKH` (`MAKH`) USING BTREE;

--
-- Indexes for table `chitiethd`
--
ALTER TABLE `chitiethd`
  ADD PRIMARY KEY (`MASOHD`,`MAMH`),
  ADD KEY `fk_cthd_MAMH` (`MAMH`) USING BTREE;

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`MASOHD`),
  ADD KEY `fk_hd_MAKH` (`MAKH`) USING BTREE;

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`MAKH`);

--
-- Indexes for table `loai`
--
ALTER TABLE `loai`
  ADD PRIMARY KEY (`MALOAI`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`MAMH`,`TENMH`);

--
-- Indexes for table `stars`
--
ALTER TABLE `stars`
  ADD PRIMARY KEY (`productid`,`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `binhluan`
--
ALTER TABLE `binhluan`
  MODIFY `MABL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `hoadon`
--
ALTER TABLE `hoadon`
  MODIFY `MASOHD` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `khachhang`
--
ALTER TABLE `khachhang`
  MODIFY `MAKH` int(11) NOT NULL AUTO_INCREMENT COMMENT '	', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `loai`
--
ALTER TABLE `loai`
  MODIFY `MALOAI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `MAMH` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `stars`
--
ALTER TABLE `stars`
  MODIFY `productid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
