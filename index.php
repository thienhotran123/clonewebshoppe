<?php
    include "Model/giohang.php";
    include "Model/order.php";
    include "Model/rating.php";
    include "Model/class.phpmailer.php";
    session_start();
    set_include_path(get_include_path().PATH_SEPARATOR.'Model/');
    spl_autoload_extensions('.php');
    spl_autoload_register();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
    <link rel="stylesheet" href="./Conttent/css/base.css">
    <link rel="stylesheet" href="./Conttent/css/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="./Conttent/assets/fontawesome-free-5.15.4-web/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"> 
    <!-- Add the slick-theme.css if you want default styling -->
    <!-- link css phần đăng ký account --> 
    <link rel="stylesheet" href="./Conttent/css/style.css">
    <link rel="stylesheet" href="./Conttent/css/style1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
        <?php
            include "View/header.php";
        ?>
        <div class="app__container">
           <div class="grid">
                        <?php
                            $ctrl = "home";
                            // kiểm tra xem trong get có action=home không có thì gán cho $crl để điều hướng tới controller
                            // thông qua từ view/home xem chi tiết có đg dẫn index.php?action=home&act=sanpham
                            if(isset($_GET['action'])) {
                                $ctrl = $_GET['action'];
                            }
                            include "Controller/".$ctrl.".php";
                        ?>
                </div>
           </div>
        </div>
<?php
    include "View/footer.php";
?>
</body>