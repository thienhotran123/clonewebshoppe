<?php
    class HangHoa {
        var $MAMH = 0;
        var $TENMH = null;
        var $SOLUONG = 0;
        var $SLTON = 0;
        var $DONGIA = 0;
        var $HINH = null;
        var $MOTA = null;
        var $LOAI = 0;
        var $SIZE = null;
        var $MAUSAC = null;
        var $NGAYDANG = null;
        var $LOAISP = null;
        var $GIAMGIA = 0;
       

        function __construct() {}
        // select 20 sản phẩm mới nhất ra trang home
        function getListHangHoaHome () {
            $select = "select * from sanpham order by rand()limit 20";
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // bấn giảm dần theo đơn giá 
        function getListHangHoaDonGia30_60 ($start,$limit) {
            $select = "SELECT * FROM sanpham WHERE DONGIA >= 300000 AND DONGIA <= 600000 limit ".$start.",".$limit;
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // bấn tăng dần theo đơn giá 
        function getListHangHoaDonGia10_35 ($start,$limit) {
            $select = "SELECT * FROM sanpham WHERE DONGIA >= 100000 AND DONGIA <= 350000 limit ".$start.",".$limit;
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // bấn tăng dần theo đơn giá 
        function getListHangHoaDonGia60_100 ($start,$limit) {
            $select = "SELECT * FROM sanpham WHERE DONGIA >= 600000 AND DONGIA <= 1000000 limit ".$start.",".$limit;
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // slect 10 sản phẩm sale 
        function getListHangHoaSale() {
            $select = "select * from sanpham where GIAMGIA>0 ";
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // select 10 sản phẩm mới nhất
        function getListHangHoaNew () {
            $select = "select * from sanpham order by MAMH desc limit 10";
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // select 10 sản phẩm bán chạy
        function getListHangHoaBanChay () {
            $select = "SELECT sp.TENMH, sp.DONGIA, sp.IMAGES, cthd.SOLUONGMUA  FROM chitiethd cthd inner join sanpham sp
            ON cthd.MAMH=sp.MAMH
            WHERE cthd.SOLUONGMUA>0 limit 10";
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // select danh mục Áo Khoác
        function getListSanPhamAoKhoac() {
            $select = "select * from sanpham where LOAISP ='Áo Khoác'";
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // select danh mục quần
        function getListSanPhamQuan() {
            $select = "SELECT *FROM sanpham WHERE LOAISP='Quần'";
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        // select danh mục Áo
        function getListSanPhamAo() {
            $select = "select * from sanpham where LOAISP = 'Áo'";
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
        public function detail($MAMH)
        {
            $select="select * from sanpham where MAMH=$MAMH";
            $db=new connect();
            $result=$db->getInstance($select);
            return $result;
        }
        public function detailNhom($LOAI)
        {
        $select ="select * from sanpham where LOAI=:LOAI";
        // ai thực hiện câu truy vấn này query và preapre
        $db=new connect();
        $stm=$db->getListRP($select);
        // muốn preapre đc thì phải 
        $stm->bindParam(':LOAI',$LOAI);
        // $stm->bindValue(':nhom',$nhom);
        $stm->execute();
        return $stm;
        }
        public function getListDetailNhomSize($LOAI)
        {
            $select ="select distinct(SIZE) from sanpham where LOAI=:LOAI order by SIZE";
            // ai thực hiện câu truy vấn này query và preapre
            $db=new connect();
            $stm=$db->getListP($select);
            // muốn preapre đc thì phải 
            $stm->bindParam(':LOAI',$LOAI);
            // $stm->bindValue(':LOAI',$LOAI);
            $stm->execute();
            return $stm;
    
        }
        // Select Tim Kiem
        public function getListSearch($name) {
            $select = "SELECT * FROM sanpham WHERE TENMH LIKE :tenmh ";
            $db = new connect();
            $stm = $db->getListP($select);
            $stm -> bindValue(':tenmh', "%".$name."%");
            $stm -> execute();
            return $stm;
        }
        // Select Count Page
        public function getListCount() {
            $select = "SELECT COUNT(*) FROM sanpham";
            $db = new connect();
            $result = $db->getInstance($select);
            return $result[0];
        }
        // Select Count Page 10-35
        public function getListCount1() {
            $select = "SELECT COUNT(*) FROM sanpham WHERE DONGIA >= 100000 AND DONGIA <= 350000";
            $db = new connect();
            $result = $db->getInstance($select);
            return $result[0];
        }
        // Select Count Page 30-60
        public function getListCount2() {
            $select = "SELECT COUNT(*) FROM sanpham WHERE DONGIA >= 300000 AND DONGIA <= 600000";
            $db = new connect();
            $result = $db->getInstance($select);
            return $result[0];
        }
        // Select Count Page 60-100
        public function getListCount3() {
            $select = "SELECT COUNT(*) FROM sanpham WHERE DONGIA >= 600000 AND DONGIA <= 1000000";
            $db = new connect();
            $result = $db->getInstance($select);
            return $result[0];
        }
         // Select Count Page
         public function getListPage($start, $limit) {
            $select = "SELECT * FROM sanpham order by rand() LIMIT ".$start.",".$limit;
            $db = new connect();
            $result = $db->getList($select);
            return $result;
        }
    }
?>