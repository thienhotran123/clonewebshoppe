<?php
    class Star {
        var $productid = null;
        var $username = null;
        var $star = null;
        function __construct()
        {
            
        }
        function avg() { 
            $db = new connect();
            $select = "select productid,round(AVG(rating),1) avg from stars group by productid";
            $result = $db->getList($select);
            $average = [];
            while ($row = $result->fetch()) {
                $average[$row['productid']] = $row['avg'];
            }
            return $average;
        }

        function get($uid,$id) {
            $select = "select rating from stars where username='$uid' and productid=$id";
            $db = new connect();
            $result = $db->getInstance($select);
            return $result[0];
        }

        function save($id,$uid,$rating) {
            $select = "REPLACE INTO stars(productid,username,rating) values(?,?,?)";
            $db = new connect();
            $stm = $db->getListP($select);
            $stm->execute([$id,$uid,$rating]);
        }
    }
?>