<?php
$action = "account";
if (isset($_GET['act'])) {
    $action = $_GET['act'];
}
switch ($action) {
    case "account":
       
        include("View/login_register.php");
        break;

    case "create_account":
        function test_input($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        } 
        $required = "Trường này bắt buộc phải nhập."; 
        // định nghĩa các biến và set giá trị mặc định là blank 
        $name = $email = $address = $phone = $nameAccount = $password = "";
        $err = array();
        $isAction = false;
        // xác thực form bằng PHP
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //name user
            if (empty($_POST["name_user"])) {
                $err['nameErr'] = $required;
            } else {
                echo $_POST["name_user"];
                $name = test_input($_POST["name_user"]);
                if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                    $err['nameErr'] = "Chỉ cho phép các chữ cái và khoảng trắng";
                }
            }
            //address user
            if (empty($_POST["address_user"])) {
                $err['address_user'] = $required;
            } else {
                $address = test_input($_POST["address_user"]);
                if (strlen($address) < 5 || !preg_match("/^[a-zA-Z ]*$/", $address)) {
                    $err['address_user'] = "Chỉ cho phép các chữ cái, khoảng trắng và nhiều hơn 5 ký tự.";
                }
            }
            //telephone number
            if (empty($_POST["phone_user"])) {
                $err['phoneErr'] = $required;
            } else {
                $phone = test_input($_POST["phone_user"]);
                if (!preg_replace('/[^0-9]/', '', $phone)) {
                    $err['phoneErr'] = "Số điện thoại không hợp lệ.";
                }
            }
            //name acount
            if (empty($_POST["name_account"])) {
                $err['nameAccountErr'] = $required;
            } else {
                $nameAccount = test_input($_POST["name_account"]);
                if (!preg_match("/^[a-zA-Z ]*$/", $nameAccount)) {
                    $err['nameAccountErr'] = "Chỉ cho phép các chữ cái và khoảng trắng";
                }
            }
            //email
            if (empty($_POST["email"])) {
                $err['emailErr'] = $required;
            } else {
                $email = test_input($_POST["email"]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $err['emailErr'] = "Định dạng email không hợp lệ";
                }
            }
            //password 
            if (empty($_POST["password"]) || empty($_POST["password_confirmation"])) {
                $err['passwordErr'] = $required;
            } else {
                // $email = test_input($_POST["password"]); 
                // $email = test_input($_POST["password_confirmation"]); 
                if ($_POST["password"] != $_POST["password_confirmation"]) {
                    $err['passwordErr'] = "Mật khẩu nhập lại không trùng nhau.";
                }
            }


            //handle action form
            if (empty($err)) {
                $name_user = $_POST['name_user'];
                $address_user = $_POST['address_user'];
                $phone_user = $_POST['phone_user'];
                $name_account = $_POST['name_account'];
                $email = $_POST['email'];
                //md5
                $pass = $_POST['password'];
                $password = md5($pass);
                $password_confirmation = $_POST['password_confirmation'];
        
                $dt = new User();
                $dt->insertUser($name_user, $address_user, $phone_user, $name_account, $email, $password); 
                unset($_SESSION['error']);
                unset($_SESSION['valueInput']);

                echo '<script>alert("Tạo tài khoản thành công")</script>';
                echo '<meta http-equiv="refresh" content="0;url=../index.php?action=account&act=login"/>'; 
            }
            else{
                $valueInput = [ 'name'=>$name ,'email'=> $email , 'address'=>$address , 'phone'=>$phone , 'nameAccount'=>$nameAccount];

                $_SESSION['error'] = $err;
                $_SESSION['valueInput'] = $valueInput;
               
                echo '<script>alert("Tạo tài khoản thất bại")</script>';
                echo '<meta http-equiv="refresh" content="0;url=../index.php?action=account"/>'; 
            }
        }

    
    break;

    case "login":
        include("View/login.php");
        break;
    case 'login_acount':
        $user = $_POST['name_account'];
        $matkhau = $_POST['password'];

        $pass = md5($matkhau);

        $dt = new user();
        $u = $dt->loginUser($user, $pass);;
        if ($u) {
            $_SESSION['makh'] = $u[0]; //29
            $_SESSION['tenkh'] = $u[1]; //hai
            $_SESSION['diachi'] = $u[3];
            $_SESSION['dienthoai'] = $u[4];
            $_SESSION['username'] = $u[5];
            $_SESSION['email'] = $u[6];  

             
            echo '<script> alert("Đăng nhập thành công  ");</script>';
            echo '<meta http-equiv="refresh" content="0;url=../index.php"/>';
        } else {
            echo '<script> alert("Tài khoản hoặc mật khẩu không đúng.");</script>';
            echo '<meta http-equiv="refresh" content="0;url=../index.php?action=account&act=login"/>';
        }

        break;
    case 'forgotPass':
        include("View/login.php");
        break;
    case 'reset':
        $sdt = $_POST['name_account'];
        $getemail = $_POST['password'];
        $dt = new User();
        $result =  $dt->checkAccount($sdt, $getemail);

         
        if ($result == true) {
            $email = md5($result['EMAIL']);
            $pass = md5($result['PASS']);
            $_SESSION['makh_update']=$result['MAKH'];
            // tạo đường link http://localhost/index.php?action=home&act=home
            $link = "<a href='http://localhost/index.php?action=account&act=valid_reset&key=" . $email . "&reset=" . $pass . "'>Click To Reset password</a>";
            // tiến hành gửi mail
            $mail = new PHPMailer();

            $mail->CharSet =  "utf-8";

            $mail->IsSMTP();

            // enable SMTP authentication

            $mail->SMTPAuth = true;

            // GMAIL username

            $mail->Username = "phplytest20@gmail.com";

            // GMAIL password

            $mail->Password = "Phplytest20@php";

            $mail->SMTPSecure = "tls";

            // sets GMAIL as the SMTP server

            $mail->Host = "smtp.gmail.com";

            // set the SMTP port for the GMAIL server

            $mail->Port = "587";

            $mail->From = 'phplytest20@gmail.com';

            $mail->FromName = 'Ly';

            $mail->AddAddress($getemail, 'reciever_name');

            $mail->Subject  =  'Reset Password';

            $mail->IsHTML(true);

            $mail->Body    = 'Click On This Link to Reset Password ' . $link . '';
            if ($mail->Send()) {
                echo "Check Your Email and Click on the link sent to your email";
            } else {

                echo "Mail Error - >" . $mail->ErrorInfo;
            }
        }  
        // if ($result == true) {
        //     $email = md5($result['EMAIL']);
        //     $pass = md5($result['PASS']);
        //     // tạo đường link http://localhost/index.php?action=home&act=home
        //     $link = "<a href='http://localhost:82/index.php?action=account&act=valid_reset&key=" . $email . "&reset=" . $pass . "'>Click To Reset password</a>";
        //     // tiến hành gửi mail
        //     $mail = new PHPMailer();
        //     $mail->CharSet =  "utf-8";
        //     $mail->IsSMTP();
        //     // enable SMTP authentication 
        //     $mail->SMTPAuth = true; 
        //     // GMAIL username 
        //     $mail->Username = "testphpthinh@gmail.com"; 
        //     // GMAIL password 
        //     $mail->Password = "thinh2002"; 
        //     $mail->SMTPSecure = "tls"; 
        //     // sets GMAIL as the SMTP server 
        //     $mail->Host = "smtp.gmail.com"; 
        //     // set the SMTP port for the GMAIL server 
        //     $mail->Port = "587"; 
        //     $mail->From = 'testphpthinh@gmail.com'; 
        //     $mail->FromName = 'test_php thinh'; 
        //     $mail->AddAddress($getemail, 'reciever_name'); 
        //     $mail->Subject  =  'Reset Password'; 
        //     $mail->IsHTML(true); 
        //     $mail->Body    = 'Click On This Link to Reset Password ' . $link . '';
        //     if ($mail->Send()) {
        //         echo "Check Your Email and Click on the link sent to your email";
        //     } else { 
        //         echo "Mail Error - >" . $mail->ErrorInfo;
        //     }
        // } 
        else {
            echo '<script> alert("Email hoặc số điện thoại không đúng");</script>';
            echo '<meta http-equiv="refresh" content="0;url=../index.php?action=account&act=forgotPass"/>';
        }
        break;
    case 'valid_reset':
        include("View/login.php");
        break;
    case 'update_pass':
        $pass = $_POST['name_account'];
        $pass2 = $_POST['password'];

        if ($pass == $pass2) {
            $makh = $_SESSION['makh_update'];
            $password = md5($pass);
            $dt = new User();
            $result =  $dt->updatePass($password, $makh);

            echo '<script> alert("Bạn đã cập nhật mật khẩu thành công");</script>';
            echo '<meta http-equiv="refresh" content="0;url=../index.php?action=account&act=login"/>';
        } else {
            echo '<script> alert("Mật khẩu không trùng nhau");</script>';
            echo '<meta http-equiv="refresh" content="0;url=../index.php?action=account&act=forgotPass"/>';
        }
        break;
    case 'logout':

        unset($_SESSION['makh']);
        unset($_SESSION['tenkh']);
        unset($_SESSION['diachi']);
        unset($_SESSION['dienthoai']);
        unset($_SESSION['username']);
        unset($_SESSION['email']);
        unset($_SESSION['matkhau']);
        echo '<script> alert("Ban da dang xuat");</script>';
        echo '<meta http-equiv="refresh" content="0;url=../index.php"/>';

        break;

    case 'updateAvatar':
        include("View/upImg.php");
        break;
    case 'uploadImg':
        // lưu lại $_FILE
        // tạo thư mục
        $target_dir = "./Conttent/upload/";
        //vị trí file nó sẽ lưu tạm trong server($_FILE)
        //$target_file="upload/juno1.jpg"
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        // giả sử biến cờ
        $upload = 1;
        // lấy phần mở rộng đổi sang thành chữ thường 
        //$_FILES là một mảng kêt hợp các mục được tải lên v pt là POST
        // key(name(tên file),tmp_name(nơi lưu tạm nằm bất cứ đâu trong server), error: lỗi, size: kích thước)
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // kiểm tra image file có đc gửi lên hay ko
        if (isset($_POST['submit'])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                $upload = 1;
            } else {
                echo "File is not image";
                $upload = 0;
            }
        }
        //kiểm tra 1 file có tồn tại hay không
        if (file_exists($target_file)) {
            echo " File is exists";
            $upload = 0;
        }
        // kiểm tra dung lượng của hình,nếu hình lớn 500kb
        // if ($_FILES["fileToUpload"]["size"] > 500000) {
        //     echo " File is too large";
        //     $upload = 0;
        // }
        // kiểm tra xem file có phần mở rộng phải là JPG, PNG,gif, jpeg
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png" && $imageFileType != "gif") {
            echo "Only jpg...";
            $upload = 0;
        }
        //tiến hành lưu vào trong thu mục upload
        if ($upload == 0) {
            echo " File not upload";
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $img = basename($_FILES["fileToUpload"]["name"]);
                $makh = $_SESSION['makh'];
                $dt = new User();
                $dt->insertAvartar($img, $makh);

                echo '<meta http-equiv="refresh" content="0;url=../index.php"/>';
            } else {
                echo "error";
            }
        }
}
