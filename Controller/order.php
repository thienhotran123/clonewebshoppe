<?php
    $action = "order";
    switch ($action) {
        case "order":
            if(isset($_SESSION['makh'])) {
                $makhang = $_SESSION['makh'];
                $hd = new hoaDon();
                $sohoadonid = $hd->insertOrder($makhang);
                $_SESSION['sohd'] = $sohoadonid;
                $total = 0;
                foreach ($_SESSION['cart'] as $key => $item) {
                    $hd->insertOderDetail($sohoadonid, $item['mahh'], $item['soluong'],$item['size'],$item['mausac'],$item['total']);
                    $total+=$item['total'];
                }
                $hd->updateOrderTotal($sohoadonid,$total);
                include 'View/order.php';
            }
            else {
                include 'View/login.php';
            }
            break;
    }
    
?>
