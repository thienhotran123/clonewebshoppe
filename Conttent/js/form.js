//Đối tượng Validator
alert("vào")
function Validator(options) {
    //11
    function getParent(element, selector) {
        while (element.parentElement) {
            if (element.parentElement.matches(selector)) {
                return element.parentElement;
            }
            element = element.parentElement;
        }
    }


    var selectorRules = {};
    //Hàm thực hiện validate
    function validate(inputElement, rule) {
        var errorMessage;
        var errorElemet =
            getParent(inputElement, options.formGroupSelector).querySelector(options.errorSelector);
        //lấy ra các rule của selector
        var rules = selectorRules[rule.selector];
        //Lặp qua từng rule và kiểm tra
        //Nếu có lỗi thì dừng việc kiểm tra
        for (var i = 0; i < rules.length; ++i) {
            switch (inputElement.type) {
                case 'radio':
                case 'checkbox':
                    errorMessage = rules[i](
                        formElement.querySelector(rule.selector+':checked')
                    );

                    break;

                default:
                    errorMessage = rules[i](inputElement.value);
            }


            if (errorMessage) break;
        }

        if (errorMessage) {
            errorElemet.innerHTML = errorMessage;
            getParent(inputElement, options.formGroupSelector).classList.add('invalid');
        } else {
            errorElemet.innerHTML = '';
            getParent(inputElement, options.formGroupSelector).classList.remove('invalid');

        }
        return !errorMessage;
    }


    //Lấy element của form cần validate
    var formElement = document.querySelector(options.form);



    if (formElement) {
        console.log('formElement.onsubmit', formElement.onsubmit )
        formElement.onsubmit = function (e) {
            // preventDefault hủy bỏ trạng thái mặc định của submit form 
            e.preventDefault();

            var isFormValid = true;

            //Kiểm tra lại rule 
            options.rules.forEach(function (rule) {
                var inputElement = formElement.querySelector(rule.selector); 
                var isValid = validate(inputElement, rule);
                if (!isValid) {
                    isFormValid = false;
                }
            });


            var enableInput = formElement.querySelectorAll('[name]');




            if (isFormValid) {
                //Trường hợp Submit với jvascrip
                if (typeof options.onSubmit === 'function') {

                    var formValues = Array.from(enableInput).reduce(function (values, input) {
                        
                        switch (input.type) {
                            case 'radio':
                                values[input.name]= formElement.querySelector ('input[name="' + input.name + '"]:checked').value;
                                 break;
                            case 'checkbox' :
                                if(!input.matches(':checked')) {
                                    values[input.name] = '';
                                    return values;
                                }

                                if(!Array.isArray(values[input.name])){
                                    values[input.name] = [];
                                }
                                values[input.name].push(input.value);
                            
                            break;
                            case 'file':
                                values[input.name] = input.value;
                                break;

                            default :
                            values[input.name] = input.value
                        }

                        return values;
                    }, {});

                    options.onSubmit(formValues)
                }
                //trường hợp submit mặc định
                else {
                    formElement.submit();
                }
            }
        }
        //Lặp qua mỗi quy định và xử lý event
        options.rules.forEach(function (rule) {

            if (Array.isArray(selectorRules[rule.selector])) {
                selectorRules[rule.selector].push(rule.test)
            } else {
                selectorRules[rule.selector] = [rule.test]
            }

            var inputElements = formElement.querySelectorAll(rule.selector);

            Array.from(inputElements).forEach(function (inputElement) {
                //Xứ lí trường hợp blur khỏi input
                inputElement.onblur = function () {
                    validate(inputElement, rule);
                }
                //Xử lí trường hợp người dùng nhập vào input
                inputElement.oninput = function () {
                    var errorElemet =
                        getParent(inputElement, options.formGroupSelector).querySelector(options.errorSelector);
                    errorElemet.innerHTML = '';
                    getParent(inputElement, options.formGroupSelector).classList.remove('invalid');
                }
            })


        });
    }
}


//Đinh nghĩa các rules(quy tắc)
//Nguyên tắc của các rules
//1: Khi có lỗi trả ra messag lỗi
//2 : khi hợp lệ không trả ra gì cả (undefined)
Validator.isRequired = function (selector, message) {
    return {
        selector: selector,
        test: function (value) {
            return value ? undefined : message || 'Vui lòng nhập trường này '
        }
    } 
}
Validator.isEmail = function (selector, message) {
    return {
        selector: selector,
        test: function (value) {
            var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            return regex.test(value) ? undefined : message || 'Vui lòng Nhập Email ';

        }
    }
}
Validator.minLength = function (selector, min, message) {
    return {
        selector: selector,
        test: function (value) {
            return value.length >= min ? undefined : message || `Vui lòng Nhập tối thiếu ${min} kí tự`;

        }
    }
}
Validator.isConfirmed = function (selector, getConfirmValue, message) {
    return {
        selector: selector,
        test: function (value) {
            return value === getConfirmValue() ? undefined : message || 'Giá trị nhập vào không chính xác';
        }
    }
}