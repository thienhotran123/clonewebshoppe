<?php
    if(isset($_GET['act'])) {
        if($_GET['act'] == "sale") {
            $ac = 1;
        }
        elseif($_GET['act'] == "new") {
            $ac = 2;
        }
        elseif($_GET['act'] == "banchay") {
            $ac = 3;
        }
        elseif($_GET['act'] == "aokhoac") {
            $ac = 4;
        }
        elseif($_GET['act'] == "quan") {
            $ac = 5;
        }
        elseif($_GET['act'] == "ao") {
            $ac = 6;
        }
    }
?>
<div class="grid__row app__content">
    <div class="grid__column-2">
        <nav class="category">
            <h3 class="category__heading">
                <i class="category__heading-icon fas fa-list"></i>
                Danh mục
            </h3>
            <ul class="category-list">
                <li class="category-item ">
                    <a href="index.php?action=home&act=aokhoac" class="category-item__Link js-category-item__link" style="text-decoration: none;">Áo khoác</a>
                </li>
                <li class="category-item ">
                    <a href="index.php?action=home&act=quan" class="category-item__Link " style="text-decoration: none;">Quần</a>
                </li>
                <li class="category-item ">
                    <a href="index.php?action=home&act=ao" class="category-item__Link " style="text-decoration: none;">Áo Thun</a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="grid__column-10">
    <div class="home-filter">
                <span class="home-filter__labdel">Sắp xếp theo</span>
                <a href="index.php?action=home&act=sale" class="home-filter-btn btn"style="font-size:15px;">Sale</a>
                <a href="index.php?action=home&act=new" class="home-filter-btn btn "style="font-size:15px;">Mới nhất</a>
                <a href="index.php?action=home&act=banchay" class="home-filter-btn btn"style="font-size:15px;">Bán chạy</a>
                <div class="select-input">
                    <span class="select-input__label">Giá</span>
                    <i class="select-input__label-icon fas fa-angle-down"></i>
                    <!-- Selection option -->
                    <ul class="select-input-list">
                        <li class="select-input-item">
                            <a href="index.php?action=home&act=gia1" class="select-input-link" >
                            100.000đ - 350.000đ
                            </a>
                        </li>
                        <li class="select-input-item">
                            <a href="index.php?action=home&act=gia2" class="select-input-link" >
                            300.000đ - 600.000đ
                            </a>
                        </li>
                        <li class="select-input-item">
                            <a href="index.php?action=home&act=gia3" class="select-input-link" >
                            600.000đ - 1.000.000đ
                            </a>
                        </li>
                    </ul>
                </div>
            <div class="home-filter__page">
                <span class="home-filter__page-num">
                    <span class="home-filter__page-current">Welcome</span>
                </span>
            </div>
        </div>
        <div class="home-product">
                <div class="grid__row">
                    <!-- do san pham -->
                    <?php
                        $hh = new hangHoa();
                        switch ($ac) {
                            case 1:
                                $result = $hh->getListHangHoaSale ();
                                break;
                            case 2:
                                $result = $hh->getListHangHoaNew ();
                                break;
                            case 3:
                                $result = $hh->getListHangHoaBanChay ();
                                break;
                            case 4:
                                $result = $hh->getListSanPhamAoKhoac ();
                                break;
                            case 5:
                                $result = $hh->getListSanPhamQuan ();
                                break;
                            case 6:
                                $result = $hh->getListSanPhamAo ();
                                break;
                        }
                        while ($row = $result->fetch()):
                    ?>
                    
                        <div class="grid__column-2-4">
                            <!-- Product item -->
                            <a class="home-product-item" style="text-decoration:none;" href="index.php?action=home&act=sanphamchitiet&id=<?php echo $row['MAMH']?>"> 
                                <div class="home-product-item__img" style="background-image: url('./Conttent/img/<?php echo $row['IMAGES']?>');"></div>
                             <h5 class="home-product-item__name" ><?php echo $row['TENMH']?></h5>
                                <div class="home-product-item__price">
                                    <?php
                                        if($ac == 1) {
                                            echo "<span class='home-product-item__price-old'>".number_format($row['DONGIA'])."đ</span>";
                                            echo "<span class='home-product-item__price-current'>".number_format($row['GIAMGIA'])."đ</span>";
                                        }
                                        if($ac == 2 || $ac == 3) {
                                            echo "<span class='home-product-item__price-current'>".number_format($row['DONGIA'])."đ</span>";
                                        }
                                        if($ac == 4 || $ac == 5 ||$ac==6) {
                                             if($row['GIAMGIA']>0){
                                             echo "<span class='home-product-item__price-old'>".number_format($row['DONGIA'])."đ</span>";
                                             echo "<span class='home-product-item__price-current'>".number_format($row['GIAMGIA'])."đ</span>";
                                             }
                                             else{
                                                echo "<span class='home-product-item__price-current'>".number_format($row['DONGIA'])."đ</span>";
                                             }
                                           
                                        }
                                    ?>
                                    
                                </div>
                                <div class="home-product-item__action">
                                    <!-- khi bấm vào tim thì xóa class này sẽ hiện tim có màu home-product-item__like--liked  -->
                                    <span class="home-product-item__like home-product-item__like--liked">
                                        <i class="home-product-item__like-icon-empty far fa-heart"></i>
                                        <i class="home-product-item__like-icon-fill fas fa-heart"></i>
                                    </span>
                                    <div class="home-product-item__rating">
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                    </div> 
                                    <?php
                                        if($ac == 3){
                                    ?>
                                    <span class="home-product-item__sold">
                                       <?php echo "(".$row['SOLUONGMUA'].")".' đã bán';?>
                                    </span>
                                    <?php
                                        }
                                    ?>
                                </div>
                                <div class="home-product-item__origin">
                                    <span class="home-product-item__brand">Whoo</span>
                                    <span class="home-product-item__origin-name">Mixi</span>
                                </div>
                                <div class="home-product-item__favourite">
                                    <i class="fas fa-check"></i>
                                    <span>Yêu thích</span>    
                                </div>
                                <!-- hiện phần này khi là sản phẩm sale -->
                                <?php
                                    if($ac == 1)
                                        echo "<div class='home-product-item__sale-off'>
                                                <span class='home-product-item__sale-off-percent'>43%</span>
                                                <span class='home-product-item__sale-off-label'>GIẢM</span>
                                            </div>";
                                ?>
                            </a>
                        </div>
                        <?php
                            endwhile;
                        ?>
                </div>
        </div>
    </div>
</div>