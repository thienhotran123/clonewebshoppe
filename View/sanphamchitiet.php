<script type="text/javascript">
    function chonsize(a) {
        document.getElementById("size").value = a;
    }
    function chonmau(a) {
        console.log(a);
        document.getElementById("color").value = a;
    }
    var stars = {
        init: function() {
            for(let docket of document.getElementsByClassName('pstar')) {
                    for(let star of docket.getElementsByTagName('img')) {
                        star.addEventListener('click',stars.click);
                    }
            }
        },
        click:function () {
            console.log('cac')
            let all = this.parentElement.getElementsByTagName("img"),
                set = this.dataset.set,
                i = 1;
            for(let star of all) {
                star.src=i<=set?"Conttent/img/star.png":"Conttent/img/star-blank.png";
                i++;
            }
            document.getElementById("ninPdt").value = this.parentElement.dataset.pid;
            document.getElementById("ninStar").value = this.dataset.set;
            document.getElementById("ninForm_2").submit();
        }
    }
    window.addEventListener("DOMContentLoaded", stars.init)
</script>
<div class="grid__row">
                    <ul class="page_list">
                        <li class="page_list-item">
                            <a href="" class="page_list-link">Home</a>
                        </li>
                        <li class="page_list-item">
                            <a href="" class="page_list-link"> > </a>
                        </li>
                        <li class="page_list-item">
                            <a href="" class="page_list-link">Chi Tiet</a>
                        </li>
                    </ul>
</div>
<form method="post" id="ninForm_2" target="_self">
        <input type="hidden" id="ninPdt" name="pid">
        <input type="hidden" id="ninStar" name="stars">
</form>
<div class="grid__row product__detail">
        <!-- đổ hình ảnh sp -->
        <?php 
        if(isset($_GET['id'])) {
         $id=$_GET['id'];
         $db=new HangHoa();
         $result=$db->detail($id);
         $mamh=$result['MAMH'];
         $tenhh=$result['TENMH'];
         $slton = $result['SLTON'];
         $dongia=$result['DONGIA'];
         $hinh=$result['IMAGES'];
         $mota=$result['MOTA'];
         $loai=$result['LOAI'];
         $giamgia=$result['GIAMGIA'];
        }
        ?>
    <div class="grid__column-4">

        <div class="product__detail-img-big" style="background-image: url(./Conttent/img/<?php echo $hinh;?>)"></div>
        <ul class="product__detail-list">
        <?php 
            $result=$db->detailNhom($loai);
            while($set=$result->fetch()):
        ?>
            <li class="product__detail-item">
                <img src="./Conttent/img/<?php echo $set['IMAGES'];?>" style="width: 130px; height: 130px;" class="product__detail-img-small">  
            </li>
        <?php 
        endwhile;
        ?>
        </ul>
    </div>
        <!-- đổ các chi tiết của sản phẩm -->
    <div class="grid__column-6">
         <form action="index.php?action=giohang&act=add_cart"  method="post">
                <!-- tên sản phẩm -->
                <input type="hidden" name="mahh" value="<?php echo $mamh?>" />
                <div class="product__detail-name">
                   <?php echo $tenhh;?>
                </div>
                <!-- rating -->
                <?php
                    $_STARS = new Star();
                    if(isset($_SESSION['tenkh'])):
                        $uid = $_SESSION['tenkh'];
                    if(isset($_POST['pid']) && isset($_POST['stars'])) {
                        $_STARS->save($_POST['pid'],$uid,$_POST['stars']);
                    }
                    $average = $_STARS->avg();
                    $rating = $_STARS->get($uid,$id);
                ?>
                <p class="product__rating-item"><?php echo $average[$id];?>/5</p>
                <div class="home-product-item__rating product__rating-item">
                    <div class="rating">
                        <div class="pstar" data-pid="<?=$id?>">
                            <?php
                                for($i = 1; $i<=5; $i++) {
                                    $img = $i<=$rating?"star":"star-blank";
                                    echo "<img src='Conttent/img/$img.png' width='30' style='margin-bottom:7px; cursor: pointer;' data-set='$i'>";
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
                    else:
                        $average = $_STARS->avg();
                ?>
                <p class="product__rating-item">Rating: <?php echo $average[$id];?>/5</p>
                <?php endif; ?>
                <!-- giá bán -->
                <div class="product__detail-price">
                    <?php 
                    if($giamgia>0)
                    {
                       echo "<span class='product__detail-price-old'>$dongia</span>";
                       echo "<span class='product__detail-price-current'>$giamgia</span>";
                    }
                    else{
                        echo "<span class='product__detail-price-current'>$dongia</span>";
                    }
                    ?>
                  
                </div>
                <!-- mô tả -->
                <div class="product__detail-description">
                    <span class="product__detail-description-heading">Mô tả: </span>
                    <span class="product__detail-description-msg">
                      <?php 
                      echo $mota;
                      ?>
                    </span>
                </div>
                <!-- bỏ màu -->
                <input type="hidden" name="mau" id="mau" value="<?php echo $set['MAUSAC'];?>">
                <div class="product__detail-properties">
                <span class="detail__heading">Màu sắc:</span>
                <div class="product__detail-btn-wrap">
                    <?php 
                        $result=$db->detailNhom($loai);
                        while($set=$result->fetch()):
                    ?>
                   
                      <button type="button" class="btn-detail" onclick="chonmau('<?php echo $set['MAUSAC'];?>')" name="<?php echo $set['MAUSAC'];?>" 
                        value="<?php echo $set['MAUSAC'];?>"><?php echo $set['MAUSAC'];?></button>
                    <?php endwhile;?>
                    </div>
                    <input id='color' type="hidden" name='color' value=''>
                </div>
                <!-- đổ size -->
                <input type="hidden" name="size" id="size" value="<?php echo $set['SIZE'];?>">
                <div class="product__detail-properties">
                    <span class="detail__heading">Size: </span>
                    <div class="product__detail-btn-wrap">
                        <?php 
                        $result=$db->getListDetailNhomSize($loai);
                        while($set=$result->fetch()):
                        ?>
                        <button type="button" class="btn-detail" onclick="chonsize(<?php echo $set['SIZE'];?>)" name="<?php echo $set['SIZE'];?>" 
                        value="<?php echo $set['SIZE'];?>"><?php echo $set['SIZE'];?></button>
                        <?php
                        endwhile;
                        
                        ?>
                    </div>
                    
                </div>
                <!-- số lượng -->
                <div class="product__detail-properties">
                    <span class="detail__heading">Số lượng: </span>
                    <input type="number" name="soluong" id="" min="1" max="100" size="100" value="1">
                </div>
                <!-- mua ngay -->
                <button type="submit" class="btn btn-mua" style="font-size: 1.5rem;" >
                    Mua ngay
                </button>
            </form>
    </div>
</div>
<?php
if(isset($_SESSION['makh'])):
?>
    <div>
    <div class="col-12 mt-5 pb-5">
        <div class="row">
            <!-- kiểm tra thử là phải bình luận của id sản phẩm hay ko -->
          
                <div class="float-left mb-4"><h2>Bình luận: </h2></div>
                <hr>
            </div>
            <form action="index.php?action=home&act=comment&id=<?php echo $_GET['id'];?>" method="post">
            <div class="row mb-4"> 
                    <input type="hidden" name="txtmahh" value="<?php echo $_GET['id'];?>" />
                    <img src="Conttent/img/comments.jpg"   height="40px"; />
                    <textarea class="input-field" type="text" name="comment" rows="1" cols="70" id="comment" placeholder="Thêm bình luận" style="height: 36px;  ">  

                    </textarea>
                    <input type="submit" class="btn btn-primary" id="submitButton" style="float: right;  " value="Bình Luận" />                
            </div>
            
            </form>
            <div class="row">
                <div class="float-left mb-4"><h2>Các bình luận:</h2></div>
                <hr>
            </div>
            <div class="row">
                <?php
                    $id=$_GET['id'];
                    $dt=new User();
                    $results=$dt->getListComments($_GET['id']);
                    while($set=$results->fetch()):
                ?>
                <div class="col-12 mt-2 mb-2">
                    <div class="row">
                        <?php
                            if($set[8]):
                        ?>
                        <img src="./Conttent/upload/<?php echo $set[8]; ?>"  height="30px" >
                        <?php
                            else:
                        ?>
                        <img src="Conttent/img/non-login.jpg" height="30px">
                        <?php endif; ?>
                        <p style=" font-size: 16px; padding: 0px 10px;">
                            <?php  
                                echo '<b>'.$set['TENKH'].':</b>'.$set['NOIDUNG'];
                            ?>
                        </p>
                    </div>
                </div>
                <?php
                endwhile;
                ?>
               <br/>
            </div>

        </div>
            </div>
<?php endif;?>
