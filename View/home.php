<?php
if(isset($_GET['act'])) {
    if($_GET['act'] == "gia1") {
        $act = 1;
    }
    elseif($_GET['act'] == "gia2"){
        $act = 2;
    }
    elseif($_GET['act'] == "gia3"){
        $act = 3;
    }
    elseif($_GET['act'] == "search"){
        $act = 4;
    }
}  
else {
        $act = 5;
    }
?>
<!-- Phân trang -->
<?php
    $hh = new HangHoa();
    if($act == 5) {
        $count = $hh->getListCount();
    }
    elseif($act == 1) {
        $count = $hh->getListCount1();
    }
    elseif($act == 2) {
        $count = $hh->getListCount2();
    }
    elseif($act == 3) {
        $count = $hh->getListCount3();
    }
    $limit = 10;
    $p = new Page();
    $page = $p -> findPage($count,$limit);
    $currentPage = isset($_GET['page'])?$_GET['page'] : 1;
    $start = $p -> findStart($limit);
?>
<div class="grid__row app__content">
    <div class="grid__column-2">
        <nav class="category">
            <h3 class="category__heading">
                <i class="category__heading-icon fas fa-list"></i>
                Danh mục
            </h3>
            <ul class="category-list">
                <li class="category-item ">
                    <a href="index.php?action=home&act=aokhoac" class="category-item__Link js-category-item__link" style="text-decoration: none;">Áo khoác</a>
                </li>
                <li class="category-item ">
                    <a href="index.php?action=home&act=quan" class="category-item__Link " style="text-decoration: none;">Quần</a>
                </li>
                <li class="category-item ">
                    <a href="index.php?action=home&act=ao" class="category-item__Link " style="text-decoration: none;">Áo Thun</a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="grid__column-10">
        <div class="home-filter">
                <span class="home-filter__labdel">Sắp xếp theo</span>
                <a href="index.php?action=home&act=sale" class="home-filter-btn btn" style="font-size:15px;">Sale</a>
                <a href="index.php?action=home&act=new" class="home-filter-btn btn "style="font-size:15px;">Mới nhất</a>
                <a href="index.php?action=home&act=banchay" class="home-filter-btn btn"style="font-size:15px;">Bán chạy</a>
                <div class="select-input">
                    <span class="select-input__label">Giá</span>
                    <i class="select-input__label-icon fas fa-angle-down"></i>
                    <!-- Selection option -->
                    <ul class="select-input-list">
                        <li class="select-input-item">
                            <a href="index.php?action=home&act=gia1" class="select-input-link" >
                                100.000đ - 350.000đ
                            </a>
                        </li>
                        <li class="select-input-item">
                            <a href="index.php?action=home&act=gia2" class="select-input-link" >
                                    300.000đ - 600.000đ
                            </a>
                        </li>
                        <li class="select-input-item">
                            <a href="index.php?action=home&act=gia3" class="select-input-link" >
                                    600.000đ - 1.000.000đ
                            </a>
                        </li>
                    </ul>
                </div>

            <div class="home-filter__page">
                <span class="home-filter__page-num">
                    <span class="home-filter__page-current">Welcome</span>
                </span>
            </div>
        </div>
        <div class="home-product">
                <div class="grid__row">
                    <!-- đổ sản cả sale cả ko sale vào lun -->
                    <?php
                        $hh = new HangHoa();
                        if($act == 1) {
                            $result = $hh->getListHangHoaDonGia10_35($start,$limit);
                        }
                        elseif($act == 2) {
                            $result = $hh->getListHangHoaDonGia30_60($start,$limit);
                        }
                        elseif($act == 3) {
                            $result = $hh->getListHangHoaDonGia60_100($start,$limit);
                        }
                        elseif($act == 4) {
                            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                                $namesearch = $_POST['txtsearch'];
                                $result = $hh->getListSearch($namesearch);
                            }
                        }
                        elseif($act == 5) {
                            // $result = $hh->getListHangHoaHome ();
                            $result = $hh->getListPage($start,$limit);
                        }
                        while ($row = $result->fetch()):
                    ?>
                        <div class="grid__column-2-4">
                            <!-- Product item -->
                            <a class="home-product-item"style="text-decoration:none; hover;" href="index.php?action=home&act=sanphamchitiet&id=<?php echo $row['MAMH']?>">
                                <div class="home-product-item__img" style="background-image: url('./Conttent/img/<?php echo $row['IMAGES']?>');"></div>
                                <h5 class="home-product-item__name"><?php echo $row['TENMH']?></h5>
                                <div class="home-product-item__price">
                                    <?php
                                        if($row['GIAMGIA']>0) {
                                            echo "<span class='home-product-item__price-old'>".number_format($row['DONGIA'])."đ</span>";
                                            echo "<span class='home-product-item__price-current'>".number_format($row['GIAMGIA'])."đ</span>";
                                        }
                                        else {
                                            echo "<span class='home-product-item__price-current'>".number_format($row['DONGIA'])."đ</span>";
                                        }
                                    ?>
                                </div>
                                <div class="home-product-item__action">
                                    <!-- khi bấm vào tim thì xóa class này sẽ hiện tim có màu home-product-item__like--liked  -->
                                    <span class="home-product-item__like home-product-item__like--liked">
                                        <i class="home-product-item__like-icon-empty far fa-heart"></i>
                                        <i class="home-product-item__like-icon-fill fas fa-heart"></i>
                                    </span>
                                    <div class="home-product-item__rating">
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                        <i class="home-product-item__star-gold fas fa-star"></i>
                                    </div>
                                </div>
                                <div class="home-product-item__origin">
                                    <span class="home-product-item__brand">Whoo</span>
                                    <span class="home-product-item__origin-name">Mixi</span>
                                </div>
                                <div class="home-product-item__favourite">
                                    <i class="fas fa-check"></i>
                                    <span>Yêu thích</span>    
                                </div>
                                <!-- Nếu sản phẩm là giảm gia sẽ hiện giảm giá ra -->
                                <?php
                                    if($row['GIAMGIA']>0)
                                        echo "<div class='home-product-item__sale-off'>
                                                <span class='home-product-item__sale-off-percent'>43%</span>
                                                <span class='home-product-item__sale-off-label'>GIẢM</span>
                                            </div>";
                                ?>
                            </a>
                        </div>
                    <?php
                        endwhile;
                    ?>
                        <ul class="pagination home-product-pagination">
                           <?php
                                if($act == 5):
                           ?>
                            <!-- HOME -->
                             <?php
                                if($currentPage>1 && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&page='.($currentPage-1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-left"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>
                            <?php
                                for($i = 1; $i <= $page; $i++) 
                                {
                            ?>
                                <li class="pagination-item pagination-item--active">
                                    <a style="text-decoration: none;" href="index.php?action=home&page=<?php echo $i;?>" class="pagination-item__link"><?php echo $i;?></a>
                                </li>
                            <?php
                                }
                            ?>
                            <?php
                                if($currentPage<$page && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&page='.($currentPage+1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-right"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>
                            <!-- 1-3 -->

                            <?php
                                elseif($act == 1):
                            ?>
                             <?php
                                if($currentPage>1 && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&act=gia1&page='.($currentPage-1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-left"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>
                            <?php
                                for($i = 1; $i <= $page; $i++) 
                                {
                            ?>
                                <li class="pagination-item pagination-item--active">
                                    <a style="text-decoration: none;" href="index.php?action=home&act=gia1&page=<?php echo $i;?>" class="pagination-item__link"><?php echo $i;?></a>
                                </li>
                            <?php
                                }
                            ?>
                            <?php
                                if($currentPage<$page && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&act=gia1&page='.($currentPage+1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-right"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>
                            <!-- 3-6 -->

                            <?php
                                elseif($act == 2):
                            ?>
                             <?php
                                if($currentPage>1 && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&act=gia2&page='.($currentPage-1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-left"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>
                            <?php
                                for($i = 1; $i <= $page; $i++) 
                                {
                            ?>
                                <li class="pagination-item pagination-item--active">
                                    <a style="text-decoration: none;" href="index.php?action=home&act=gia2&page=<?php echo $i;?>" class="pagination-item__link"><?php echo $i;?></a>
                                </li>
                            <?php
                                }
                            ?>
                            <?php
                                if($currentPage<$page && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&act=gia2&page='.($currentPage+1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-right"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>
                            <!-- 600 - 1tr -->
                            <?php
                                elseif($act == 3):
                            ?>
                             <?php
                                if($currentPage>1 && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&act=gia3&page='.($currentPage-1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-left"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>
                            <?php
                                for($i = 1; $i <= $page; $i++) 
                                {
                            ?>
                                <li class="pagination-item pagination-item--active">
                                    <a style="text-decoration: none;" href="index.php?action=home&act=gia3&page=<?php echo $i;?>" class="pagination-item__link"><?php echo $i;?></a>
                                </li>
                            <?php
                                }
                            ?>
                            <?php
                                if($currentPage<$page && $page>1)
                                 {
                                     echo '<li class="pagination-item">
                                                <a href="index.php?action=home&act=gia3&page='.($currentPage+1).'" class="pagination-item__link">
                                                    <i class="pagination-item__icon fas fa-angle-right"></i>
                                                </a>
                                            </li>';
                                 }
                            ?>    

                            <?php
                                endif;
                            ?>
                            </ul>
                            
                </div>
        </div>
    </div>
</div>