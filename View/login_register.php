<?php

    // // định nghĩa các biến và set giá trị mặc định là blank 
    $name = $email = $address = $phone = $nameAccount = $password = "";
    // var_dump($_SESSION['valueInput']);
    $err=$_SESSION['error'];
    if(isset($_SESSION['valueInput'])){
        $name =$_SESSION['valueInput']['name'];
        $email =$_SESSION['valueInput']['email'];
        $address =$_SESSION['valueInput']['address'];
        $phone =$_SESSION['valueInput']['phone']; 
        $nameAccount =$_SESSION['valueInput']['nameAccount'];  
    }

?> 
<div class="next_prev-groups">
    <div class="container">
        <div class="breadcrumb-content">
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="#"> > </a></li>
                <li><a href="login_account.html">My Account</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="wrapper-container wrapper-body">
    <div class="custom-acc-page container">
        <div class="page-header">
            <h2>
                Đăng ký
            </h2>
            <span>Đăng ký tài khoản trên ứng dụng TNG-Shop</span>
        </div>
        <div class="part_login">
            <div class="wrapper-login-customer" id="validate_form">
                <form id="form-1" action="index.php?action=account&act=create_account" method="POST">
                    <!-- 
                - $ _SERVER "PHP_SELF" là một biến siêu toàn cục và sẽ gửi dữ liệu biểu mẫu đã gửi đến chính trang đó.
                - htmlspecialchars chuyển đổi các ký tự đặc biệt thành các thực thể HTML, vd:<và> bằng & lt; và & gt ; giúp tránh khỏi tin tặc tấn công 
                -->

                    <div class="wrapper_login ">
                        <div class="login_lep">
                            <div class="wrapper-content">
                                <div class="returning-customer">
                                    <div class="wrapper">
                                        <div class="form-group">
                                            <div class="text_top">
                                                <div class="text">
                                                    <span> Tên khách hàng </span>
                                                    <em>*</em>
                                                </div>
                                            </div>
                                            <input class="text_1" name="name_user" type="text" id="email_user" placeholder='Tên khách hàng' value="<?= $name; ?>">
                                            <div class="error"> <?php echo $err['nameErr']; ?></div>
                                            <div class="text_top">
                                                <div class="text">
                                                    <span>Địa chỉ</span>
                                                    <em>*</em>
                                                </div>
                                            </div>
                                            <input class="text_1" value="<?= $address; ?>" name="address_user" type="text" id="PW_user" placeholder='Địa chỉ'>
                                            <div class="error"> <?php echo $err['address_user']; ?></div>
                                            <div class="text_top">
                                                <div class="text">
                                                    <span>Số điện thoại</span>
                                                    <em>*</em>
                                                </div>
                                            </div>
                                            <input class="text_1" value="<?= $phone; ?>" name="phone_user" type="text" id="PW_user" placeholder='Số điện thoại'>
                                            <div class="error"> <?php echo $err['phoneErr']; ?></div>
                                            <div class="text_top">
                                                <div class="text">
                                                    <span>Tên tài khoản</span>
                                                    <em>*</em>
                                                </div>
                                            </div>
                                            <input class="text_1" value="<?= $nameAccount; ?>" type="text" id="setName" name='name_account' placeholder="Tên tài khoản">
                                            <div class="error"> <?php echo $err['nameAccountErr']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <form action=""> -->
                        <div class="login_right">
                            <div class="wrapper-content">
                                <div class="returning-customer">
                                    <div class="wrapper">
                                        <div class="form-group">
                                        </div>

                                        <div class="form-group">
                                            <div class="text_top">
                                                <div class="text">
                                                    <span>Email</span>
                                                    <em>*</em>
                                                </div>

                                                <input class="text_1" value="<?= $email; ?>" id='setEmail' type="text" name="email" placeholder="VD: email@domain.com">
                                                <div class="error"><?php echo $err['emailErr']; ?></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="text_top">
                                                <div class="text">
                                                    <span>Password</span>
                                                    <em>*</em>
                                                </div>

                                                <!-- <input class="text_1" type="password" id="setPassword" placeholder='Nhập mật khẩu'> -->
                                                <input class="text_1" id='password' type="password" name="password" placeholder="Password">
                                                <div class="error"> <?php echo $err['passwordErr']; ?></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="text_top">
                                                <div class="text">
                                                    <span>Xác nhận Password</span>
                                                    <em>*</em>
                                                </div>
                                                <input class="text_1" id='password_confirmation' type="password" name="password_confirmation" placeholder="Xác nhận Password">
                                                <div class="form_message"></div>
                                            </div>
                                        </div>
                                        <div class="action-btn">
                                            <button class="btn btn-outline-primary form-submit" id="btnClick">
                                                Đăng ký
                                            </button>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 