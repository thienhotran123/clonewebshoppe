<div class="grid__row">
<?php
      if(!isset($_SESSION['makh']) &&  count($_SESSION['cart'])==0):
        echo '<script>alert("Bạn cần đăng nhập trước khi thanh toán")</script>';
        include "login.php";
?>
<?php
    else:
?>
                    <ul class="page_list">
                        <li class="page_list-item">
                            <a href="" class="page_list-link">Home</a>
                        </li>
                        <li class="page_list-item">
                            <a href="" class="page_list-link"> > </a>
                        </li>
                        <li class="page_list-item">
                            <a href="" class="page_list-link">Hóa đơn</a>
                        </li>
                    </ul>
</div>
<div class="grid__row" style="height: 600px; margin-top: 12px">
    <form action="" method="post" class="oder__form">
    <?php
          if(isset($_SESSION['sohd'])) {
            $masohd = $_SESSION['sohd'];
            $hd = new hoaDon();
            $result = $hd->getOrder($masohd);
            $ngay = $result['NGAYDAT'];
            $ten = $result['TENKH'];
            $diachi = $result['DIACHI'];
            $sdt = $result['DIENTHOAI'];
          }
    ?>
        <div class="oder__address-border--top"></div>
        <div class="oder__address">
            <div class="oder__address-heading">
                <i class="oder__address-heading-icon fas fa-map-marker-alt"></i>
                <span class="oder__address-heading-msg">Địa Chỉ Nhận Hàng</span>
            </div>
           <div class="order__address-body">
                    <!-- Họ và tên -->
                <div class="oder__address-name">
                    <span><?php echo $ten?></span>
                </div>
                <!-- số điện thoại -->
                <div class="oder__address-phone">
                    <span>(+84)</span>
                    <!-- bỏ số điẹn thoại vào đây -->
                    <span><?php echo $sdt?></span>
                </div>
                <!-- địa chỉ -->
                <div class="oder__address-specific">
                    <span><?php echo $diachi?></span>
                </div>
           </div>
        </div>
        <div class="order__product">
            <div class="order__product-header">
                <span>Sản phẩm</span>
                <span>Màu</span>
                <span>Size</span>
                <span>Đơn giá</span>
                <span> Số Lượng</span>
            </div>
            <div class="order__product-bonus">
                <i class="fas fa-vest order__product-bonus-icon1"></i>
                <span class="order__product-bonus-shop">TNG STORE</span>
                <i class="far fa-comments order__product-bonus-icon2"></i>
                <span class="order__product-bonus-chat">Chat ngay</span>
            </div>

            <!-- ĐỔ thông tin sản phảm vào đây  -->
            <?php
                $result = $hd->getOderDeital($masohd);
                while($row = $result->fetch()):
            ?>
            <div class="order__product-detail">
                <div class="order__product-detail-wrap">
                    <img style="border-radius: 2px;" src="../Conttent/img/<?php echo $row['IMAGES']?>" alt="" width="40px" height="40px">
                    <span class="order__product-detail-msg">
                        <span class="order__product-detail-submsg"><?php echo $row['TENMH']?></span> 
                    </span>
                </div>
                <span class="order__product-detail-color"><?php echo $row['MAUSAC']?></span>
                <span class="order__product-detail-size"><?php echo $row['SIZE']?></span>
                <span class="order__product-detail-price"><?php echo $row['DONGIA']?> VNĐ</span>
                <span class="order__product-detail-quantity"><?php echo $row['SOLUONGMUA']?></span>
            </div>
            <?php endwhile;?>

            
            <div class="order__product-ship">
                <div class="order__product-ship-head">
                    <span class="order__product-ship-msg">Lời nhắn:</span>
                    <div class="order__product-ship-import">
                        <div class="order__product-ship-contain">
                            <input type="text" class="order__product-ship-input" placeholder="Lưu ý cho người bán">
                        </div>
                    </div>
                </div>
                <div class="order__product-ship-option">
                    <div class="order__product-ship-option-voicher">
                        <i class="order__product-ship-option-voicher-icon fas fa-ticket-alt"></i>
                        <span class="order__product-ship-option-voicher-msg">Shopee Voucher</span>
                    </div>
                    <div class="order__product-ship-option-coin">
                        <i class="order__product-ship-option-coin-icon fas fa-donate"></i>
                        <span class="order__product-ship-option-coin-msg">Shopee Xu</span>
                    </div>
                    <div class="order__product-ship-option-fast">
                        <i class="order__product-ship-option-fast-icon fas fa-shipping-fast"></i>
                        <span class="order__product-ship-option-fast-msg">Shopee ship</span>
                    </div>  
                </div>
            </div>
            <div class="order__product-pay">
                <span  class="order__product-pay-msg">Tổng thanh toán:</span>
                <span  class="order__product-pay-money"><?php echo getsup_total();?> VNĐ</span>
            </div>
        </div>
    </form>
    <?php endif;?>
</div>





