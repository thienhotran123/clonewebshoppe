<?php 
 if(!isset($_SESSION['cart'])||count($_SESSION['cart'])==0):
   echo "<script>alert('chưa có hàng')</script>";
   include "home.php";
   ?>
   <?php 
   else:
   ?>
 <form action="" method="post">
<div id="page_payment" class="container">
        <div class="page-header header-title">
            <h1>
                <span>Shopping Cart</span>
            </h1>
        </div>
        <div id="shopCarts" class="row">
            <div class="col-sm-8 col-12 ">
                <div class="box-title">
                    <p>Product</p>
                </div>
                <ul class="cart-list">
                <?php 
                       foreach($_SESSION['cart'] as $key=>$item):
                       $costnew=number_format($item['dongia']);
                       $totalnew=number_format($item['total']);
                        ?>
                    <li class="wrap_icon">
                        <div class="details row ">
                            <div class="cart-thumb col-sm-4">
                                <a class="product-img">
                                    <img src= <?php echo 'Conttent/img/'.$item['hinh']; ?>>
                                </a>
                            </div>
                            <div class="wrap_contents col-sm-8">
                                <div class="name_product">
                                    <?php  echo $item['tenmh']?>
                                </div> 
                                <div class="cart-details">
                                    <div class="price-box">
                                        <span class="money">Đơn Gía:<?php echo  $costnew?></span>
                                    </div>
                                    <div class="price-box">
                                        <span class="money">Size:<?php echo  $item['size']?></span>
                                    </div>
                                    <div class="price-box">
                                        <span class="money">Color:<?php echo  $item['mausac']?></span>
                                    </div>
                                </div>
                                <div class="quantity ">
                                    <label for="">Quantity :<?php  echo $item['soluong']?> </label>
                                    <div class="row">
                                        <div class="qty-group col-sm-4">
                                            <div class="quantity">
                                                <div class="btn-group mb-3">
        
                                                    <button style="min-width:0px" type="button" class="btn btn-outline-secondary btn_dash" id='btn-dash'>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                            fill="currentColor" class="bi bi-dash" viewBox="0 0 16 16">
                                                            <path
                                                                d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z" />
                                                        </svg>
                                                    </button>
                                                    <input 
                                                               id='numberPd'

                                                       type="text" class="btn btn-outline-secondary number_pd" value='1'
                                                        style=" text-align: center;width: 70px; font-size: 16px;min-width:0px">
                                                    <button style="min-width:0px" type="button" id='btn-plus' class="btn btn-outline-secondary btn_plus">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                            fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                                            <path
                                                                d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                                        </svg>
                                                    </button>
                                                </div>
        
        
                                            </div>
                                        </div>
                                        <div class="group-action col-sm-8">
                                            <button style="min-width:0px" type="button" class="btn btn-outline-secondary update_cart btn_cart">UPDATE CART</button>
                                            <a href="index.php?action=giohang&act=delete_item&id=<?php echo $item['mahh'];?>"><button style="min-width:0px" type="button" class="btn btn-outline-danger remove_cart btn_cart">REMOVE</button></a>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </li>
                    <?php endforeach;?>
                </ul>
                <div class="continue_cart">
                <a href="index.php?action=home">
                    <button style="min-width:0px" type="button" class="btn btn-outline-success special-select btn_next_index"> Continue Shopping </button>
                </a>
                </div>
                <div class="secure-shopping-guarantee" style="margin-bottom: 30px;">
                    <img src="./Conttent/img/w3_bn1.webp"  >
                </div> 

            </div>
            <div class="col-sm-4 col-12">
                <div class="box-title">
                    <p>Subtotal</p>
                </div>
                <div class="total">
                    <span class="price">
                        <span>$</span>
                        <span class="money">
                        <?php echo getsup_total();?>
                        </span>
                    </span>
                </div>
                <div class="terms_conditions_checkout">
                    <div class="terms_conditions">
                        <input type="checkbox" class="conditions">
                        <span> 
                            I agree with the terms and conditions
                        </span>
                    </div>
                    <div class="btn-actions">
                       <button style="min-width:0px" id="card_info" class='btn btn-danger disagree_conditions' style="padding:6px 46px">
                            <a href="index.php?action=order" style="text-decoration: none; color:#fff;">
                                Send Info Card
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <?php endif;?>
    <script>

        var btnDash = document.getElementById('btn-dash');

        var btnPlus = document.getElementById('btn-plus');
        

        // var number = document.getElementsByClassName('number_pd').value;



        btnDash.onclick = function(){



            var number = document.getElementById('numberPd');

            var total = 0



            total =  number.value * 1 - 1;

            number.value = total;



        }

        btnPlus.onclick = function(){

            var number = document.getElementById('numberPd');

            var total = 0



            total =  number.value * 1 + 1;

            number.value = total;

        }

    </script>