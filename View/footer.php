<!-- Footer -->
<footer class="footer">
            <div class="grid">
                <div class="grid__row">
                    <div class="grid__column-2-4">
                        <h3 class="footer__heading">Chăm sóc khách hàng</h3>
                        <ul class="footer-list">
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Trung tâm trợ giúp</a>
                            </li>
                            <li class="footer-item">
                                <li class="footer-item"><a href="" class="footer-item__link">TNG-Shop Mal</a>
                            </li>
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Hướng dẫn mua hàng</a>
                            </li>
                        </ul>
                    </div>
                    <div class="grid__column-2-4">
                        <h3 class="footer__heading">Giới Thiệu</h3>
                        <ul class="footer-list">
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Giới thiệu TNG-shop</a>
                            </li>
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Tuyển dụng</a>
                            </li>
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Điều khoản</a>
                            </li>
                        </ul>
                    </div>
                    <div class="grid__column-2-4">
                        <h3 class="footer__heading">Danh mục</h3>
                        <ul class="footer-list">
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Áo hoodie</a>
                            </li>
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Khô gà MixiFood</a>
                            </li>
                            <li class="footer-item">
                                <a href="" class="footer-item__link">Quần thung </a>
                            </li>
                        </ul>
                    </div>
                    <div class="grid__column-2-4">
                        <h3 class="footer__heading">Theo dõi</h3>
                        <ul class="footer-list">
                            <li class="footer-item">
                                <a href="" class="footer-item__link">
                                    <i class="footer-item__link-icon fab fa-facebook"></i>
                                    Facebook
                                </a>
                            </li>
                            <li class="footer-item">
                                <a href="" class="footer-item__link">
                                    <i class="footer-item__link-icon fab fa-instagram"></i>
                                    Instagram
                                </a>
                            </li>
                            <li class="footer-item">
                                    <a href="" class="footer-item__link">
                                        <i class="footer-item__link-icon fab fa-linkedin"></i>
                                        Linkedin
                                    </a>
                                </li>
                        </ul>
                    </div>
                    <div class="grid__column-2-4">
                        <h3 class="footer__heading">Liên kết với app</h3>
                        <div class="footer__dowload">
                            <img src="../Conttent/img/QR.png" alt="qr" class="footer__dowload-qr">
                            <div class="footer__dowload-apps">
                                <a href="" class="footer__dowload-app">
                                    <img src="../Conttent/img/app_store.png" alt="app-store" class="footer__dowload-app-img">
                                </a>
                                <a href="" class="footer__dowload-app">
                                    <img src="../Conttent/img/google_play.png" alt="google-play" class="footer__dowload-app-img">
                                </a>
                                <a href="" class="footer__dowload-app">
                                    <img src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/1ae215920a31f2fc75b00d4ee9ae8551.png" alt="AppGalley" class="footer__dowload-app-img">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="grid">
                    <div class="grid__row">
                        <div class="footer__bottom-shared">
                            <div class="footer__bottom-introduce-item">
                                <a href="" class="footer__bottom-introduce-link">Chính sách bảo mật</a>
                            </div>
                            <div class="footer__bottom-introduce-item">
                                <a href="" class="footer__bottom-introduce-link">Quy chế hoạt động</a>
                            </div>
                            <div class="footer__bottom-introduce-item">
                                <a href="" class="footer__bottom-introduce-link">Chính sách vận chuyển</a>
                            </div>
                            <div class="footer__bottom-introduce-item">
                                <a href="" class="footer__bottom-introduce-link">Chính sách trả hàng và hoàn tiền</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid__row">
                        <div class="footer__bottom-shared">
                            <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=18375">
                                <div class="footer__bottom-background footer-vn-vn_registered_red-png footer__bottom-icon1"></div>
                            </a>
                            <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=18375">
                                <div class="footer__bottom-background footer-vn-vn_registered_red-png footer__bottom-icon1"></div>
                            </a>
                            <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=18375">
                                <div class="footer__bottom-background footer-vn-vn_no_fake_item-png footer__bottom-icon2"></div>
                            </a>
                        </div>
                    </div>
                    <div class="grid__row">
                        <span class="footer__bottom-shared footer__bottom-company">Công ty TNG Shopee</span>
                    </div>
                    <div class="grid__row">
                        <span class="footer__bottom-shared footer__bottom-wrap">Địa chỉ: Tầng 4-5-6, Tòa nhà Capital Place, số 29 đường Liễu Giai, Phường Ngọc Khánh, Quận Ba Đình, Thành phố Hà Nội, Việt Nam. Tổng đài hỗ trợ: 19001221 - Email: cskh@hotro.shopee.vn</span>
                    </div>
                    <div class="grid__row">
                        <span class="footer__bottom-shared footer__bottom-wrap">Chịu Trách Nhiệm Quản Lý Nội Dung: Nguyễn Đức Trí - Điện thoại liên hệ: 024 73081221 (ext 4678)</span>
                    </div>
                    <div class="grid__row">
                        <span class="footer__bottom-shared footer__bottom-wrap">Mã số doanh nghiệp: 0106773786 do Sở Kế hoạch & Đầu tư TP Hà Nội cấp lần đầu ngày 10/02/2015</span>
                    </div>
                    <div class="grid__row">
                        <span class="footer__bottom-shared footer__bottom-copyright">© 2021 - Bản quyền thuộc về Công ty TNG Shopee</span>
                    </div>
                </div>
            </div>
        </footer>

    </div>